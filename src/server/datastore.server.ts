import { Players } from '@rbxts/services';

import datastore from '@rbxts/datastore2';

datastore.Combine('PlayerInformation', 'score');

const sessionData = new Map<number, PlayerData>();

Players.PlayerAdded.Connect(async (player: Player) => {
    const scoreStore = datastore('score', player);
    let score: number = scoreStore.Get() as number;

    print(score);
    const leaderStats = new Instance('Folder');
    leaderStats.Name = 'leaderstats';

    const scoreLabel = new Instance('NumberValue');
    scoreLabel.Name = 'Score';
    scoreLabel.Value = score;
    scoreLabel.Parent = leaderStats;

    scoreStore.OnUpdate((newValue) => {
        score = newValue as number;
        scoreLabel.Value = score;
    });

    leaderStats.Parent = player;
});

class PlayerData {
    private score: Number;
    private timesDied: Number;

    constructor() {
        this.score = 0;
        this.timesDied = 0;
    }

    setScore(score: Number) {
        this.score = score;
    }

    getScore() {
        return this.score;
    }
}
