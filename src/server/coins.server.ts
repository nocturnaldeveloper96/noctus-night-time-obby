import { Workspace, Players } from '@rbxts/services';
import datastore from '@rbxts/datastore2';
const COIN_WORTH = 10;
const allObjects: Instance[] = Workspace.GetChildren();

allObjects.forEach((obj: Instance) => {
    if (obj.Name === 'Coin') {
        const part = obj as Part;
        part.Touched.Connect((otherPart: BasePart) => {
            const humanoid = otherPart.Parent?.FindFirstChildWhichIsA('Humanoid');
            if (!humanoid) return;
            const player: Player | undefined = Players.GetPlayerFromCharacter(otherPart.Parent);
            if (!player) return;
            print(player);
            const scoreStore = datastore('score', player);
            scoreStore.Increment(COIN_WORTH);
            part.CanCollide = false;
            part.Destroy();
            return;
        });
    }
});

export {};
