const trap = game.Workspace.FindFirstChild('Trap') as Part;
trap.Touched.Connect((otherPart: BasePart) => {
    const humanoid = otherPart.Parent?.FindFirstChildWhichIsA('Humanoid');
    if (humanoid) humanoid.Health = 0;
});

export {};
