const allObjects: Instance[] = game.Workspace.GetChildren();
const JUMP_BOOST = 120;

// Loop through all the objects in the workspace
allObjects.forEach((obj: Instance) => {
    // If it matches the boosters folder
    if (obj.Name === 'Boosters') {
        const boosters: Part[] = obj.GetChildren() as Part[];
        // Loop through each booster
        boosters.forEach((booster: Part) => {
            // Add a touched event to each booster
            booster.Touched.Connect((otherPart: BasePart) => {
                // Get the 'thing' that collided with this booster
                const humanoid = otherPart.Parent?.FindFirstChildWhichIsA('Humanoid');
                if (!humanoid) return;

                // If its a human, full send.
                const currentJumpPower = humanoid.JumpPower;
                booster.CanCollide = false;
                if (currentJumpPower < JUMP_BOOST) {
                    humanoid.JumpPower = JUMP_BOOST;
                    booster.Destroy();
                    wait(10);
                    humanoid.JumpPower = currentJumpPower;
                }
            });
        });
    }
});

export {};
